using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneController : ISceneController
{
    private MainPanel mainPanel;
    private PlayerShipFactory playerShipFactory;
    private PlayerShip playerShip;
    private GameController gameController;

    private bool isResuming;

    public GameSceneController(MainPanel mainPanel, PlayerShipFactory playerShipFactory)
    {
        this.mainPanel = mainPanel;
        this.playerShipFactory = playerShipFactory;
        isResuming = true;
        AddEvents();
    }

    private void AddEvents()
    {
        mainPanel.ShipSelected += OnShipSelected;
    }

    private void OnShipSelected(int shipIndex)
    {
        SetShipSelectionMenuActive(false);
        InitializePlayerShip(shipIndex);
        StartGame();
    }

    private void InitializePlayerShip(int shipIndex)
    {
        playerShip = playerShipFactory.CreateShip(shipIndex);
    }

    public void StartScene()
    {
        InitializePlayerShips();
        InitializeEnemies();
        SetShipSelectionMenuActive(true);
    }

    private void InitializePlayerShips()
    {
        throw new NotImplementedException();
    }

    private void InitializeEnemies()
    {

    }

    private void SetShipSelectionMenuActive(bool value)
    {
        mainPanel.SetShipSelectionMenuActive(value);
    }

    private void StartGame()
    {

    }

    private void OnPauseButtonPressed()
    {
        isResuming = !isResuming;
        Time.timeScale = isResuming ? 1f : 0;
    }

    private void OnExitButtonPressed()
    {

    }
}
