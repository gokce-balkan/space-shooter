using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T>
{
    private Queue<T> pool;

    //private static ObjectPool instance;
    //public static ObjectPool Instance
    //{
    //    get
    //    {
    //        if (instance == null)
    //        {
    //            instance = new ObjectPool();
    //        }
    //        return instance;
    //    }
    //}

    public void CreatePool(GameObject prefab, int count)
    {
        pool = new Queue<T>(count);
        for (int i = 0; i < count; ++i)
        {
            GameObject poolObject = Object.Instantiate(prefab);
            poolObject.name = typeof(T).ToString() + " " + i;
            pool.Enqueue(poolObject.GetComponent<T>());
        }
    }

    public T GetNextAvailableObject()
    {
        return pool.Dequeue();
    }

    public void QueueObject(T poolObject)
    {
        pool.Enqueue(poolObject);
    }
}
