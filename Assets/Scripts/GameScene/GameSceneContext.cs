using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneContext : MonoBehaviour, ISceneContext
{
    [SerializeField] private MainPanel mainPanel;

    private GameSceneController gameSceneController;
    private GameController gameController;
    private PlayerShipFactory playerShipFactory;

    public void BuildScene()
    {
        playerShipFactory = new PlayerShipFactory();
        gameSceneController = new GameSceneController(mainPanel, playerShipFactory);
    }

    public ISceneController GetSceneController()
    {
        return gameSceneController;
    }
}
