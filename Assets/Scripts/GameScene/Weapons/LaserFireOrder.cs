﻿using UnityEngine;

[System.Serializable]
public struct LaserFireOrder
{
    public LaserGun lasergun;
    public Vector3 directionAngle;

    public void Execute()
    {
        lasergun.Fire(directionAngle);
    }
}