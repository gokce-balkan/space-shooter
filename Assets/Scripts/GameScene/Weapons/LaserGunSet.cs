using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserGunSet : Weapon
{
    [SerializeField] private List<LaserFireOrder[]> laserFireOrderList;
    public int Power { set; get; }

    public void Fire()
    {
        LaserFireOrder[] laserFireOrders = laserFireOrderList[GetFireOrderIndex()];

        for (int i = 0; i < laserFireOrders.Length; ++i)
        {
            laserFireOrders[i].Execute();
        }
    }

    private int GetFireOrderIndex()
    {
        return Power - 1;
    }
}
