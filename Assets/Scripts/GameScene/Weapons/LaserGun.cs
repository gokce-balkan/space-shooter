using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserGun : MonoBehaviour
{
    private ObjectPool<Laser> laserPool;
    private Transform laserGunTransform;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        laserGunTransform = transform;
        //laserPool.CreatePool()
    }



    public void Fire(Vector3 directionAngle)
    {
        Laser laser = laserPool.GetNextAvailableObject();
        //Laser laser = ObjectPool<Laser>.Instance.GetNextAvailableObject();
        //laser.SetPosition(position);
        //laser.SetRotation(directionAngle);
        laser.Launch();
    }
}
