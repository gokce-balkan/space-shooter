using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

[TestFixture]
public class SceneChangeTests
{
    [UnityTest]
    public IEnumerator SceneChangesFromStartSceneToGameScene()
    {
        yield return SceneManager.LoadSceneAsync("StartScene");
        IPlayButtonView playButtonView = Substitute.For<IPlayButtonView>();
        PlayButtonController playButtonController = new PlayButtonController(playButtonView);
        SoundController soundController = new SoundController(Substitute.For<ISoundOptionsView>());
        StartSceneController startSceneController = new StartSceneController(playButtonController, soundController);
        playButtonView.PlayButtonPressed();
        var waitForScene = new WaitForSceneLoaded("GameScene");
        yield return waitForScene;
        Assert.AreEqual("GameScene", SceneManager.GetActiveScene().name);
    }
}
