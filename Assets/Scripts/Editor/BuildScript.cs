using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public static class BuildScript
{
    private const char CommandStartCharacter = '-';
    private const string BuildPathCommand = "-buildPath";
    private const string KeystorePasswordCommand = "-keystorePass";
    private const string KeyAliasPasswordCommand = "-keyaliasPass";
    private const string ProvisionProfileId = "-provisionUUID";

    [MenuItem("Build/Build Android")]
    public static void BuildAndroid()
    {
        PlayerSettings.Android.useCustomKeystore = true;
        EditorUserBuildSettings.buildAppBundle = false;

        // Set bundle version. NEW_BUILD_NUMBER environment variable is set in the codemagic.yaml 
        var versionIsSet = int.TryParse(Environment.GetEnvironmentVariable("NEW_BUILD_NUMBER"), out int version);
        if (versionIsSet)
        {
            Debug.Log($"Bundle version code set to {version}");
            PlayerSettings.Android.bundleVersionCode = version;
        }
        else
        {
            Debug.Log("Bundle version not provided");
        }

        // Set keystore name
        string keystoreName = Environment.GetEnvironmentVariable("CM_KEYSTORE_PATH");
        if (!String.IsNullOrEmpty(keystoreName))
        {
            Debug.Log($"Setting path to keystore: {keystoreName}");
            PlayerSettings.Android.keystoreName = keystoreName;
            Debug.Log($"Keystore set to: {PlayerSettings.Android.keystoreName}");
        }
        else
        {
            Debug.Log("Keystore name not provided");
        }

        // Set keystore password
        string keystorePass = Environment.GetEnvironmentVariable("CM_KEYSTORE_PASSWORD");
        if (!String.IsNullOrEmpty(keystorePass))
        {
            Debug.Log("Setting keystore password");
            PlayerSettings.Android.keystorePass = keystorePass;
        }
        else
        {
            Debug.Log("Keystore password not provided");
        }

        // Set keystore alias name
        string keyaliasName = Environment.GetEnvironmentVariable("CM_KEY_ALIAS");
        if (!String.IsNullOrEmpty(keyaliasName))
        {
            Debug.Log("Setting keystore alias");
            PlayerSettings.Android.keyaliasName = keyaliasName;
        }
        else
        {
            Debug.Log("Keystore alias not provided");
        }

        // Set keystore password
        string keyaliasPass = Environment.GetEnvironmentVariable("CM_KEY_PASSWORD");
        if (!String.IsNullOrEmpty(keyaliasPass))
        {
            Debug.Log("Setting keystore alias password");
            PlayerSettings.Android.keyaliasPass = keyaliasPass;
        }
        else
        {
            Debug.Log("Keystore alias password not provided");
        }
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.locationPathName = "android/spaceshooter.apk";
        buildPlayerOptions.target = BuildTarget.Android;
        buildPlayerOptions.options = BuildOptions.None;
        buildPlayerOptions.scenes = GetEnabledScenePaths();

        Debug.Log("Building Android");
        BuildPipeline.BuildPlayer(buildPlayerOptions);
        Debug.Log("Built Android");
    }


        /*
        public static void BuildAndroid()
        {
            string buildPath, keystorePassword, keyAliasPassword, androidType;
            //Parse custom command line arguments
            Dictionary<string, string> commandToValueDictionary = GetCommandLineArguments();
            commandToValueDictionary.TryGetValue(BuildPathCommand, out buildPath);
            commandToValueDictionary.TryGetValue(KeystorePasswordCommand, out keystorePassword);
            commandToValueDictionary.TryGetValue(KeyAliasPasswordCommand, out keyAliasPassword);
            //Update Key Store and Alias password
            //PlayerSettings.keyaliasPass = keyAliasPassword;
            //PlayerSettings.keystorePass = keystorePassword;
            buildPath = string.IsNullOrEmpty(buildPath) ? "Builds/Android/spaceshooter.apk" : buildPath;
            //BuildPipeline.BuildPlayer(GetEnabledScenePaths(), buildPath, BuildTarget.Android, BuildOptions.None);
            BuildPipeline.BuildPlayer(GetEnabledScenePaths(), buildPath, BuildTarget.Android, BuildOptions.Development);
        }*/

        [MenuItem("Builds/iOS")]
    public static void BuildiOS()
    {
        string buildPath, provisionUUID;
        //Parse command line arguments
        Dictionary<string, string> commandToValueDictionary = GetCommandLineArguments();
        commandToValueDictionary.TryGetValue(BuildPathCommand, out buildPath);
        commandToValueDictionary.TryGetValue(ProvisionProfileId, out provisionUUID);
        //Update iOS Manual provisioning profile to Developer or App Store
        PlayerSettings.iOS.iOSManualProvisioningProfileID = provisionUUID;
        BuildPipeline.BuildPlayer(GetEnabledScenePaths(), buildPath, BuildTarget.iOS, BuildOptions.None);
    }

    private static string[] GetEnabledScenePaths()
    {
        return EditorBuildSettings.scenes.Select(e => e.path).ToArray();
    }

    private static Dictionary<string, string> GetCommandLineArguments()
    {
        Dictionary<string, string> commandToValueDictionary = new Dictionary<string, string>();
        string[] args = Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i].StartsWith(CommandStartCharacter.ToString(), StringComparison.Ordinal))
            {
                string command = args[i];
                string value = string.Empty;
                if (i < args.Length - 1 && !args[i + 1].StartsWith(CommandStartCharacter.ToString(), StringComparison.Ordinal))
                {
                    value = args[i + 1];
                    i++;
                }
                if (!commandToValueDictionary.ContainsKey(command))
                    commandToValueDictionary.Add(command, value);
            }
        }
        return commandToValueDictionary;
    }
}
