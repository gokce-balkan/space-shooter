using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.TestTools.TestRunner.Api;
using UnityEngine;

public static class TestScript
{
    private static TestRunnerApi _runner = null;
    private class MyCallbacks : ICallbacks
    {

        public void RunStarted(ITestAdaptor testsToRun)
        { }

        public void RunFinished(ITestResultAdaptor result)
        {
            // export test results to JUnit XML format if
            // using -testsOutput argument
            Debug.Log(Application.dataPath);
            string reportPath = Path.Combine(Application.dataPath, "tests.xml");
            //string[] args = System.Environment.GetCommandLineArgs();
            //for (int i = 0; i < args.Length; i++)
            //{
            //    if (args[i] == "-testsOutput")
            //    {
            //        reportPath = args[i + 1];
            //        break;
            //    }
            //}

            if (reportPath != null)
            {
                Reporter.ReportJUnitXML(reportPath, result);
            }

            // clean up and return the proper exit code
            _runner.UnregisterCallbacks(this);
            if (result.ResultState != "Passed")
            {
                Debug.Log("Tests failed :(");
            }
            else
            {
                Debug.Log("Tests passed :)");
            }
        }

        public void TestStarted(ITestAdaptor test)
        { }

        public void TestFinished(ITestResultAdaptor result)
        { }
    }

    [MenuItem("Tests/Run Tests")]
    public static void RunUnitTests()
    {
        _runner = ScriptableObject.CreateInstance<TestRunnerApi>();
        Filter filter = new Filter()
        {
            testMode = TestMode.EditMode
        };
        _runner.RegisterCallbacks(new MyCallbacks());
        _runner.Execute(new ExecutionSettings(filter));
    }
}
