using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneController : ISceneController
{
    private readonly string GAME_SCENE_NAME = "GameScene";

    private PlayButtonController playButtonController;
    private SoundController soundController;

    public StartSceneController(PlayButtonController playButtonController, SoundController soundController)
    {
        this.playButtonController = playButtonController;
        this.soundController = soundController;
        playButtonController.PlayButtonPressed += OnPlayButtonPressed;
    }

    public void StartScene()
    {
        StartBackgroundMusic();
    }

    private void StartBackgroundMusic()
    {
        soundController.StartMusic();
    }

    private void OnPlayButtonPressed()
    {
        StartGame();
    }

    private void StartGame()
    {
        SceneManager.LoadScene(GAME_SCENE_NAME);
    }
}
