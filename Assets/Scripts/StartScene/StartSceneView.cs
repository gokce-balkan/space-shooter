using System;
using UnityEngine;
using UnityEngine.UI;

public class StartSceneView : MonoBehaviour
{
    public Action StartButtonPressed;

    [SerializeField]
    private Button startButton;

    public void Initialize()
    {
        startButton.onClick.AddListener(OnStartButtonPressed);
    }

    private void OnStartButtonPressed()
    {
        StartButtonPressed();
    }
}
