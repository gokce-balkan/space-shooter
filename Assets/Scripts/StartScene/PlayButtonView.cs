﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayButtonView : MonoBehaviour, IPlayButtonView
{
    public Action PlayButtonPressed { set; get; }

    [SerializeField] private Image playButtonImage;
    [SerializeField] private Sprite playSprite;
    [SerializeField] private Sprite loadingSprite;

    public void OnPress()
    {
        ChangeSpriteToLoading();
        PlayButtonPressed();
    }

    public void ChangeSpriteToLoading()
    {
        playButtonImage.sprite = loadingSprite;
    }
}