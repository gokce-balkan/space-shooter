using System;
using UnityEngine;
using UnityEngine.UI;

public class SoundOptionsView : MonoBehaviour, ISoundOptionsView
{
    [SerializeField] private Image sfxButton;
    [SerializeField] private Image musicButton;

    [SerializeField] private Sprite sfxOnSprite;
    [SerializeField] private Sprite sfxOffSprite;
    [SerializeField] private Sprite musicOnSprite;
    [SerializeField] private Sprite musicOffSprite;

    public Action SfxButtonPressed { set; get; }
    public Action MusicButtonPressed { set; get; }

    public void Initialize()
    {
        sfxButton.GetComponent<Button>().onClick.AddListener(() => SfxButtonPressed?.Invoke());
        musicButton.GetComponent<Button>().onClick.AddListener(() => MusicButtonPressed?.Invoke());
    }

    public void SetSoundIconsActive(bool settinsOn)
    {
        sfxButton.gameObject.SetActive(settinsOn);
        musicButton.gameObject.SetActive(settinsOn);
    }

    public void SetSfxIconMute(bool isMute)
    {
        sfxButton.sprite = isMute ? sfxOffSprite : sfxOnSprite;
    }

    public void SetMusicIconMute(bool isMute)
    {
        musicButton.sprite = isMute ? musicOffSprite : musicOnSprite;
    }
}
