using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSceneContext : MonoBehaviour, ISceneContext
{
    [SerializeField] private PlayButtonView playButtonView;
    [SerializeField] private SoundOptionsView soundOptionsView;
    [SerializeField] private AudioSource backgroundMusicSource;

    private PlayButtonController playButtonController;
    private SoundController soundController;
    private StartSceneController startSceneController;

    public void BuildScene()
    {
        playButtonController = new PlayButtonController(playButtonView);
        soundController = BuildSoundController(soundOptionsView);
        startSceneController = new StartSceneController(playButtonController, soundController);
    }

    private SoundController BuildSoundController(SoundOptionsView soundOptionsView)
    {
        SoundController soundController = new SoundController(soundOptionsView);
        soundController.AddBackgroundMusicSource(backgroundMusicSource);
        return soundController;
    }

    public ISceneController GetSceneController()
    {
        return startSceneController;
    }
}
