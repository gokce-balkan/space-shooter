﻿using System;

public class PlayButtonController
{
    public Action PlayButtonPressed;

    private IPlayButtonView playButtonView;

    public PlayButtonController(IPlayButtonView playButtonView)
    {
        this.playButtonView = playButtonView;
        playButtonView.PlayButtonPressed += OnPlayButtonPressed;
    }

    private void OnPlayButtonPressed()
    {
        PlayButtonPressed?.Invoke();
        ChangeSpriteToLoading();
    }

    private void ChangeSpriteToLoading()
    {
        playButtonView.ChangeSpriteToLoading();
    }
}