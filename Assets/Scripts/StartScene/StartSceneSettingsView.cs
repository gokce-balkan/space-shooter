using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartSceneSettingsView : MonoBehaviour
{
    [SerializeField] private Button settingsButton;
    [SerializeField] private Animator settingsAnimator;
    [SerializeField] private SoundOptionsView soundOptionsView;

    private bool settinsOn = false;

    public void ToggleSettingsButton()
    {
        settinsOn = !settinsOn;
        soundOptionsView.SetSoundIconsActive(settinsOn);
        settingsAnimator.SetTrigger(settinsOn ? "Open" : "Close");
    }
}
