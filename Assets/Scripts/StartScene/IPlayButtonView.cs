using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayButtonView
{
    Action PlayButtonPressed { get; set; }

    void ChangeSpriteToLoading();
}
