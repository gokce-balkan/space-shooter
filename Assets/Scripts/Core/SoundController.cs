using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController
{
    private Dictionary<string, AudioSource> sfxSourceDictionary;
    private AudioSource backgroundMusicSource;
    private ISoundOptionsView soundOptionsView;

    public bool SfxActive
    {
        set
        {
            PlayerPrefs.SetInt("SfxOn", value ? 1 : 0);
            foreach (var soundSource in sfxSourceDictionary)
            {
                soundSource.Value.mute = !value;
            }
            SetSfxMuteUI(!value);
        }
        get => PlayerPrefs.GetInt("SfxOn") == 1;
    }

    public bool BackgroundMusicActive
    {
        set
        {
            PlayerPrefs.SetInt("MusicOn", value ? 1 : 0);
            if (backgroundMusicSource != null)
            {
                backgroundMusicSource.mute = !value;
            }
            SetBackgroundMusicMuteUI(!value);
        }
        get => PlayerPrefs.GetInt("MusicOn") == 1;
    }

    public void StartMusic()
    {
        backgroundMusicSource.Play();
        backgroundMusicSource.mute = !BackgroundMusicActive;
    }

    public SoundController(ISoundOptionsView soundOptionsView)
    {
        SetSoundOptionsView(soundOptionsView);
        sfxSourceDictionary = new Dictionary<string, AudioSource>();
    }

    public void Initialize()
    {
        SfxActive = true;
        BackgroundMusicActive = true;
    }

    public void SetSoundOptionsView(ISoundOptionsView soundOptionsView)
    {
        this.soundOptionsView = soundOptionsView;
        soundOptionsView.Initialize();
        soundOptionsView.SfxButtonPressed += OnSfxButtonPressed;
        soundOptionsView.MusicButtonPressed += OnMusicButtonPressed;
    }

    private void OnSfxButtonPressed()
    {
        SfxActive = !SfxActive;
    }

    private void OnMusicButtonPressed()
    {
        BackgroundMusicActive = !BackgroundMusicActive;
    }

    public void AddBackgroundMusicSource(AudioSource backgroundMusicSource)
    {
        this.backgroundMusicSource = backgroundMusicSource;
    }

    public void AddSfxSource(string sfxName, AudioSource sfxSource)
    {
        sfxSourceDictionary[sfxName] = sfxSource;
    }

    public void RemoveSfxSource(AudioSource sfxSource)
    {
        RemoveSfxSource(sfxSource.name);
    }

    public void RemoveSfxSource(string sfxName)
    {
        sfxSourceDictionary.Remove(sfxName);
    }

    private void SetSfxMuteUI(bool sfxActive)
    {
        soundOptionsView.SetSfxIconMute(sfxActive);
    }

    private void SetBackgroundMusicMuteUI(bool backgroundMusicMute)
    {
        soundOptionsView.SetMusicIconMute(backgroundMusicMute);
    }
}
