﻿public interface ISceneContext
{
    void BuildScene();
    ISceneController GetSceneController();
}