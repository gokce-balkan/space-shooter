using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum SceneType
{
    Start,
    Game
}

public class AppController : MonoBehaviour
{
    private Dictionary<SceneType, string> sceneDictionary;
    private SceneType currentScene;

    public static AppController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = FindObjectOfType<AppController>();

            DontDestroyOnLoad(gameObject);
            SetSceneDictionary();

            SetCurrentScene(SceneType.Start);
            StartCurrentScene();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void SetSceneDictionary()
    {
        sceneDictionary = new Dictionary<SceneType, string>()
        {
            { SceneType.Start, "StartScene" },
            { SceneType.Game, "GameScene" },
        };
    }

    private void SetCurrentScene(SceneType scene)
    {
        currentScene = scene;
    }

    private void StartCurrentScene()
    {
        ISceneContext currentSceneContext = GameObject.FindGameObjectWithTag("Context").GetComponent<ISceneContext>();
        currentSceneContext.BuildScene();
        ISceneController sceneController = currentSceneContext.GetSceneController();
        sceneController.StartScene();
    }
}
