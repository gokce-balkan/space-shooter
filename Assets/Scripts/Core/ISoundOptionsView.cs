using System;

public interface ISoundOptionsView
{
    Action SfxButtonPressed { get; set; }
    Action MusicButtonPressed { get; set; }

    void SetSfxIconMute(bool sfxActive);

    void SetMusicIconMute(bool backgroundMusicActive);
    void Initialize();
}
