using System.Collections;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;

[TestFixture]
public class SoundTests
{
    private SoundController soundController;
    private AudioSource backgroundMusicSource;
    private AudioSource missileSfxSource;
    private AudioSource damageSfxSource;

    [SetUp]
    public void Initialize()
    {
        ISoundOptionsView soundOptionsView = Substitute.For<ISoundOptionsView>();
        soundController = new SoundController(soundOptionsView);
        backgroundMusicSource = new GameObject("BackgroundMusic").AddComponent<AudioSource>();
        missileSfxSource = new GameObject("MissileSfx").AddComponent<AudioSource>();
        damageSfxSource = new GameObject("DamageSfx").AddComponent<AudioSource>();
        soundController.AddBackgroundMusicSource(backgroundMusicSource);
        soundController.AddSfxSource("missile", missileSfxSource);
        soundController.AddSfxSource("damage", damageSfxSource);
    }

    [Test]
    public void TurnOnBackgroundMusic()
    {
        soundController.BackgroundMusicActive = true;
        Assert.IsFalse(backgroundMusicSource.mute);
    }

    [Test]
    public void TurnOffBackgroundMusic()
    {
        soundController.BackgroundMusicActive = false;
        Assert.IsTrue(backgroundMusicSource.mute);
    }

    [Test]
    public void TurnOnSfx()
    {
        soundController.SfxActive = true;
        Assert.IsFalse(missileSfxSource.mute);
        Assert.IsFalse(damageSfxSource.mute);
    }

    [Test]
    public void TurnOffSfx()
    {
        soundController.SfxActive = false;
        Assert.IsTrue(missileSfxSource.mute);
        Assert.IsTrue(damageSfxSource.mute);
    }

    [OneTimeTearDown]
    public void CleanUp()
    {
        Object.DestroyImmediate(backgroundMusicSource.gameObject);
        Object.DestroyImmediate(missileSfxSource.gameObject);
        Object.DestroyImmediate(damageSfxSource.gameObject);
    }
}
